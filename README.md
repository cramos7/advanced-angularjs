# Advanced AngularJs #

Learn to organize your code and evolve with Angular JS, raising the standard of your use of this great framework maintained by Google.

## Installation ##

**Requeriments:**

1. [NPM](https://www.npmjs.com/ "Go to homepage NPM")
2. [Gulp](http://gulpjs.com/ "Go to homepage Gulp")
3. [Browserify](http://browserify.org/ "Go to homepage Browserify")
4. [PHP](http://php.net/ "Go to homepage PHP")

**To install the dependencies:**

1. Inside the folder run the command "npm install".

2. To start the server from project.

   * In the root folder of the project run the command "gulp" or "gulp default".

   * In a new tab of the terminal execute the command "php -S localhost: 8080 post.php".

3. To access and application home page, go to [localhost:8000(http://localhost:800)] in your browser.